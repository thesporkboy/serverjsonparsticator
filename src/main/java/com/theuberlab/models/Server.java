package com.theuberlab.models;

/**
 * Represents a server as found in servers.json.
 *
 * A sample server looks like the following:
 * 	{
 * 	  "hostname": "web01",
 * 	  "Info": {
 * 	    "OS": "AML",
 * 	    "RAM": 128,
 * 	    "HDD": 1000,
 * 	    "IP": "10.10.5.10",
 * 	    "SERVICES": [
 * 	      "apache",
 * 	      "sshd"
 * 	    ],
 * 	    "OPENPORTS": [
 * 	      80,
 * 	      443,
 * 	      22
 * 	    ],
 * 	    "STATUS": "up"
 * 	  }
 * 	}
 *
 * Created by TheSporkboy on 10/13/16.
 */
public class Server {
	private String hostname;

	private ServerInfo Info;

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public ServerInfo getInfo() {
		return Info;
	}

	public void setInfo(ServerInfo info) {
		Info = info;
	}
}
