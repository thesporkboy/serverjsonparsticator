package com.theuberlab.models;

import java.util.List;

/**
 * A POJO to represent the list of servers found in servers.json.
 *
 * Created by TheSporkboy on 10/13/16.
 */
public class ServerList {
	private List<Server> servers;

	public List<Server> getServers() { return servers; }

	public void setServers(List<Server> servers) {
		this.servers = servers;
	}

}
