package com.theuberlab.models;

/**
 * A POJO to represent the server info field in servers.json
 *
 * Created by TheSporkboy on 10/13/16.
 */
public class ServerInfo {

	//TODO: Change these arrays to list so that we can do things like openports.contains(22)
	private String OS;
	//TODO: Confirm the nature of the data in these fields. I might be able to get away with an int if the sending system will reliably be sending while numbers like the example.
	private double RAM;
	private double HDD;
	//TODO: Change this to something IP specific to make subnet calculations easier?
	private String IP;
	//TODO: Evaluate the worthiness of creating an enum for this might be some with in confirming open ports match expected values. Won't work most places but maybe we're more consistent than everyone else on the internet.
	private String[] SERVICES;
	private int[] OPENPORTS;
	//TODO: Update this to use a boolean for efficiency. We're a string here initially to simplfy getter/setter code.
	private String STATUS;

	public String getOS() {
		return OS;
	}

	public void setOS(String OS) {
		this.OS = OS;
	}

	public double getRAM() {
		return RAM;
	}

	public void setRAM(double RAM) {
		this.RAM = RAM;
	}

	public double getHDD() {
		return HDD;
	}

	public void setHDD(double HDD) {
		this.HDD = HDD;
	}

	public String getIP() {
		return IP;
	}

	public void setIP(String IP) {
		this.IP = IP;
	}

	public String[] getSERVICES() {
		return SERVICES;
	}

	public void setSERVICES(String[] SERVICES) {
		this.SERVICES = SERVICES;
	}

	public int[] getOPENPORTS() {
		return OPENPORTS;
	}

	public void setOPENPORTS(int[] OPENPORTS) {
		this.OPENPORTS = OPENPORTS;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String STATUS) {
		this.STATUS = STATUS;
	}
// Here's a sample info section of a server definition
//	{
//		"OS": "AML",
//		"RAM": 128,
//		"HDD": 1000,
//		"IP": "10.10.5.10",
//		"SERVICES": [
//			"apache",
//			"sshd"
//		],
//		"OPENPORTS": [
//			80,
//			443,
//			22
//      	],
//		"STATUS": "up"
//	}
}
