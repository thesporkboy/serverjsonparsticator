package com.theuberlab;

import com.theuberlab.models.Server;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

/**
 * This is my answer to pre-screening question #2.
 *
 */
public class App {
    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(App.class);
	private final static String outputFileName = "infosec_audit";

    public static void main( String[] args ) {
		logger.debug("Starting serverjsonparsticator");

        // Grab the line separator for the OS since we'll be writing a file.
        String LS = System.getProperty("line.separator");
        File inFile = null;
        String fullFile = null;


        // Check to see if we have recieved a file name on the command line.
        if (0 < args.length) {
            inFile = new File(args[0]);
			if (!inFile.exists() || inFile.isDirectory()) {
				logger.error("File does not exist or is a directory:");
				System.exit(1);
			}
        } else {
            logger.error("Invalid arguments count: [{}]", args.length);
            System.exit(1);
        }

        // Read the file contents. Wrap it and send it on.
        try {
			logger.debug("Reading input data from file [{}]", args[0]);

            BufferedReader bReader = new BufferedReader(new FileReader(inFile));
            StringBuilder builtString = new StringBuilder();
            String line = bReader.readLine();

            while (line != null) {
                builtString.append(line);
                builtString.append(LS);
                line = bReader.readLine();
            }

			//TODO: I should probably use "elements" instead of servers
			// Check to see if the file starts with a square bracket and make it proper JSON if it does so that GSON does not complain.
			if (builtString.toString().startsWith("[")) {
				logger.debug("First line does not start with { wrapping text");
				fullFile = "{" + LS + "  \"servers\": " + builtString.toString() + LS + "}";
			} else {
				fullFile = builtString.toString();
			}

            bReader.close();
        } catch (FileNotFoundException e) {
            logger.error("Failed to read input file: File Not Found [{}]", e.getMessage());
            logger.debug("Full stack {}", e.getStackTrace());
            System.exit(1);
        } catch (IOException e) {
            logger.error("Failed to read input file: IO Error [{}]", e.getMessage());
            logger.debug("Full stack {}", e.getStackTrace());
            System.exit(1);
        }

        JsonParser parser = new JsonParser(fullFile);

        logger.debug("Parsing complete");

		//TODO: Update this to take the cidr block from the command line.
		logger.info("Down Servers:");
		for (Server thisServer : parser.getServersByStatus("down")) {
			logger.info("Server [{}] is down: Other servers within /30", thisServer.getHostname());

			List<Server> atRiskServers = parser.getServersByCIDR(thisServer.getInfo().getIP() + "/30");

			for (Server thatServer : atRiskServers) {
				//Skip the down server
				if (!thatServer.getHostname().equals(thisServer.getHostname())) {
					logger.info("Server [{}] at IP [{}]", thatServer.getHostname(), thatServer.getInfo().getIP());
				}
			}
		}

		//TODO: Update this to take OS from command line.

		List<Server> aMLServers = parser.getServersByOS("AML");
		int numAMLServers = aMLServers.size();

		int numCentOSServers = parser.getServersByOS("CENTOS").size();

		logger.info("Count of Servers by OS type AML [{}] CentOS [{}]", numAMLServers, numCentOSServers);

		List<Server> securityIssueServers = parser.getServersByOpenPort(23);

		PrintWriter writer = null;
		try {
			writer = new PrintWriter(outputFileName, "UTF-8");
		} catch (FileNotFoundException e) {
			logger.error("Failed to open output file: [{}]", e.getMessage());
			logger.debug("Full stack {}", e.getStackTrace());
			System.exit(1);
		} catch (UnsupportedEncodingException e) {
			logger.error("Failed to open output file: [{}]", e.getMessage());
			logger.debug("Full stack {}", e.getStackTrace());
			System.exit(1);
		}

		//TODO: Update this to take risky ports from the command line.
//		logger.info("Servers listening on telnet port (23)");
		for (Server thisServer : securityIssueServers) {
//			logger.info("Server [{}] OS [{}]", thisServer.getHostname(), thisServer.getInfo().getOS());

			String outLine = "Server [" + thisServer.getHostname() + "] OS [" + thisServer.getInfo().getOS() + "]" + LS;
			writer.write(outLine);
		}
		writer.close();
	}
}
