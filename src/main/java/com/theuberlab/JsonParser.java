package com.theuberlab;

import com.google.gson.Gson;
import com.theuberlab.models.Server;
import com.theuberlab.models.ServerList;
import org.apache.commons.net.util.SubnetUtils;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Parses JSON output from servers.json (with the added servers element wrapped around it.)
 *
 * Created by TheSporkboy on 10/13/16.
 */
public class JsonParser {
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(JsonParser.class);

	ServerList servers;

	/**
	 * Creates an instance of JsonParse with the specified data.
	 * Note: The input must be properly formatted JSON or the GSON parser will be unable to extract the data.
	 *
	 * @param jsonData
	 */
	public JsonParser(String jsonData) {
		logger.debug("Parsing JSON Input");
		logger.trace(jsonData);

		Gson gson = new Gson();

		servers = gson.fromJson(jsonData, ServerList.class);

		for (Server thisServer : servers.getServers()) {
			logger.debug("Parsed server: Hostname[{}] IP[{}]", thisServer.getHostname(), thisServer.getInfo().getIP());
		}

	}

	/**
	 * Returns a list of servers for the specified status.
	 *
	 * @param status
	 * @return
	 */
	public List<Server> getServersByStatus(String status) {
		List<Server> matchedServers = new ArrayList<Server>();

		for (Server thisServer : servers.getServers()) {
			if (thisServer.getInfo().getSTATUS().equals(status)) {
				matchedServers.add(thisServer);
			}
		}
		return matchedServers;
	}

	/**
	 * Returns a list of servers for the specfied subnet.
	 * Subnet must be specified in CIDR notation.
	 *
	 * @param subnet
	 * @return
	 */
	public List<Server> getServersByCIDR(String subnet) {
		List<Server> matchedServers = new ArrayList<Server>();

		SubnetUtils utils = new SubnetUtils(subnet);

		SubnetUtils.SubnetInfo subnetInfo = utils.getInfo();
		String lowAddress = subnetInfo.getLowAddress();
		String highAddress = subnetInfo.getHighAddress();

		for (Server thisServer : servers.getServers()) {
			if (utils.getInfo().isInRange(thisServer.getInfo().getIP())) {
				matchedServers.add(thisServer);
			}
		}

		return matchedServers;
	}

	/**
	 * Returns a list of servers for the given OS type.
	 *
	 * @param os
	 * @return
	 */
	public List<Server> getServersByOS(String os) {
		logger.debug("Finding servers by OS [{}]", os);
		List<Server> matchedServers = new ArrayList<Server>();

		for (Server thisServer : servers.getServers()) {
			String thisOS = thisServer.getInfo().getOS();
			if (thisOS.equals(os)) {
				matchedServers.add(thisServer);
			}
		}

		return matchedServers;
	}

	/**
	 * Returns a list of servers which are listening on the specified port.
	 *
	 * @param port
	 * @return
	 */
	public List<Server> getServersByOpenPort(int port) {
		List<Server> matchedServers = new ArrayList<Server>();

		for (Server thisServer : servers.getServers()) {
			int[] openPorts = thisServer.getInfo().getOPENPORTS();

			for (int i = 0; i < openPorts.length; i++) {
				if (openPorts[i] == port) {
					matchedServers.add(thisServer);
				}
			}
		}

		return matchedServers;
	}

	/**
	 * Returns all servers found in the JSON file.
	 *
	 * @return
	 */
	public List<Server> getAllServers() {
		return servers.getServers();
	}
}
