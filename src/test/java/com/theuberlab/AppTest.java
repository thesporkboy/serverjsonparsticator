package com.theuberlab;

import com.theuberlab.models.Server;
import junit.framework.TestCase;

import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {

	String servers = "{\n" +
			"  \"servers\": \n" +
			"[\n" +
			"  {\n" +
			"    \"hostname\": \"web01\",\n" +
			"    \"Info\": {\n" +
			"      \"OS\": \"AML\",\n" +
			"      \"RAM\": 128,\n" +
			"      \"HDD\": 1000,\n" +
			"      \"IP\": \"10.10.5.9\",\n" +
			"      \"SERVICES\": [\n" +
			"        \"apache\",\n" +
			"        \"sshd\"\n" +
			"      ],\n" +
			"      \"OPENPORTS\": [\n" +
			"        80,\n" +
			"        443,\n" +
			"        22\n" +
			"      ],\n" +
			"      \"STATUS\": \"down\"\n" +
			"    }\n" +
			"  },\n" +
			"  {\n" +
			"    \"hostname\": \"web02\",\n" +
			"    \"Info\": {\n" +
			"      \"OS\": \"AML\",\n" +
			"      \"RAM\": 128,\n" +
			"      \"HDD\": 1000,\n" +
			"      \"IP\": \"10.10.5.10\",\n" +
			"      \"SERVICES\": [\n" +
			"        \"apache\",\n" +
			"        \"sshd\"\n" +
			"      ],\n" +
			"      \"OPENPORTS\": [\n" +
			"        80,\n" +
			"        443,\n" +
			"        666\n" +
			"      ],\n" +
			"      \"STATUS\": \"up\"\n" +
			"    }\n" +
			"  }," +
			"  {\n" +
			"    \"hostname\": \"web03\",\n" +
			"    \"Info\": {\n" +
			"      \"OS\": \"CENTOS\",\n" +
			"      \"RAM\": 128,\n" +
			"      \"HDD\": 1000,\n" +
			"      \"IP\": \"10.10.5.15\",\n" +
			"      \"SERVICES\": [\n" +
			"        \"apache\",\n" +
			"        \"sshd\"\n" +
			"      ],\n" +
			"      \"OPENPORTS\": [\n" +
			"        80,\n" +
			"        443,\n" +
			"        22\n" +
			"      ],\n" +
			"      \"STATUS\": \"up\"\n" +
			"    }\n" +
			"  }\n" +
			"]\n" +
			"}\n";

    public void testGetServersInRange() throws Exception {

        JsonParser parser = new JsonParser(servers);

		boolean rightServer = false;
		boolean wrongServer = false;

        for (Server thisServer : parser.getServersByStatus("down")) {

            List<Server> atRiskServers = parser.getServersByCIDR(thisServer.getInfo().getIP() + "/30");


            for (Server thatServer : atRiskServers) {
				if (thatServer.getInfo().getIP().equals("10.10.5.10")) {
					rightServer = true;
				} else if (thatServer.getInfo().getIP().equals("10.10.5.15")) {
					wrongServer = true;
				}
            }

        }
        assertTrue("right server not found in results", rightServer);
		assertTrue("Wrong server found in results", !wrongServer);
    }

	public void testGetServersByStatus() throws Exception {
		JsonParser parser = new JsonParser(servers);

		boolean rightServer = false;
		boolean wrongServer = false;

		List<Server> downServers = parser.getServersByStatus("down");
		for (Server thisServer : downServers) {
			if (thisServer.getHostname().equals("web01")) {
				rightServer = true;
			} else if (thisServer.getHostname().equals("web02")) {
				wrongServer = true;
			}

		}

		assertTrue("right server not found in results", rightServer);
		assertTrue("Wrong server found in results", !wrongServer);
	}

	public void testGetServersByCIDR() throws Exception {

	}

	public void testGetServersByOS() throws Exception {
		JsonParser parser = new JsonParser(servers);

		boolean rightAMLServer = false;
		boolean wrongAMLServer = false;
		boolean rightCENTOSServer = false;
		boolean wrongCENTOSServer = false;

		List<Server> aMLServers = parser.getServersByOS("AML");
		for (Server thisServer : aMLServers) {
			if (thisServer.getHostname().equals("web01")) {
				rightAMLServer = true;
			} else if (thisServer.getHostname().equals("web03")) {
				wrongAMLServer = true;
			}
		}

		List<Server> centOSServers = parser.getServersByOS("CENTOS");
		for (Server thisServer : centOSServers) {
			if (thisServer.getHostname().equals("web03")) {
				rightCENTOSServer = true;
			} else if (thisServer.getHostname().equals("web02")) {
				wrongCENTOSServer = true;
			}
		}

		assertTrue("right server not found AML in results", rightAMLServer);
		assertTrue("Wrong server found in AML results", !wrongAMLServer);
		assertTrue("right server not found CENTOS in results", rightCENTOSServer);
		assertTrue("Wrong server found in CENTOS results", !wrongCENTOSServer);

//		web01 is AML web03 is CENTOS
	}

	public void testGetServersByOpenPort() throws Exception {
		JsonParser parser = new JsonParser(servers);

		boolean rightServer = false;
		boolean wrongServer = false;

		List<Server> port22Servers = parser.getServersByOpenPort(666);
		for (Server thisServer : port22Servers) {
			if (thisServer.getHostname().equals("web02")) {
				rightServer = true;
			} else if (thisServer.getHostname().equals("web03")) {
				wrongServer = true;
			}
		}

		List<Server> port80Servers = parser.getServersByOpenPort(80);

		assertTrue("Not enough servers found for port 80", port80Servers.size() == 3);

		assertTrue("right server not found in results", rightServer);
		assertTrue("Wrong server found in results", !wrongServer);
//		web02 has 666 all three have 80
	}
}
