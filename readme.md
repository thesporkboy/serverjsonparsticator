# About

Provides human output from servers.json

# Building

The project is built with maven. cd to the project directory and run:

```
mvn package
```

# Usage

building the application produces an executable jar file. It takes a single argument which is the path to the input file. 
To run this with java

```
java -jar serverjsonparsticator-1.0.0.jar servers.json
```

A sample servers.json is provided in the project's ./src/DevFiles/. If you are testing the project you will likely run:

```
> mvn clean package
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Server JSON Parsticator 1
[INFO] ------------------------------------------------------------------------
<snip>
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.036 s
[INFO] Finished at: 2016-10-13T20:48:28-07:00
[INFO] Final Memory: 20M/215M
[INFO] ------------------------------------------------------------------------
> java -jar target/serverjsonparsticator-1.0.0.jar ./src/DevFiles/servers.json 
Down Servers:
Server [web09] is down: Other servers within /30
Server [web10] at IP [10.10.5.18]
Server [database03] is down: Other servers within /30
Server [database01] at IP [10.10.6.1]
Server [database02] at IP [10.10.6.2]
Count of Servers by OS type AML [10] CentOS [4]
> ls
bitbucket-pipelines.yml		pom.xml				serverjsonparsticator.iml	target/
infosec_audit			readme.md			src/
> cat infosec_audit 
Server [web05] OS [AML]
Server [database04] OS [CENTOS]
```

# License
[Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/)
